# 
# DOWNLOAD REMOTE FILES if not done already
# 
retrieve_remote_files <- function() {
    apply(AppMetadata, 1, .retrieve_file)
    return()  # Return NULL to avoid printing everything to log
}

# Get a file for a covariate from somewhere
.retrieve_file <- function(covariate) {
    source = covariate["source"]
    
    if (source == "url") {
        .download_with_logic(covariate)
    } else if (source == "getData") {
        .handle_get_data(covariate)
    } else if (source == "ee") {
        # The `.retrieve_earth_engine_for` function uses `.download_with_logic` after 
        # we've generated the URL and destfile from the given attributes
        .retrieve_earth_engine_for(covariate["destfile"])
    }
}

# Check file exists, and dowload if not Requires a thing with a $url and a $destfile
.download_with_logic <- function(covariate) {
    
    source_files_path <- get_input_files_path()
    
    destination_file = file.path(source_files_path, covariate["destfile"])
    
    if (!file.exists(destination_file) | (file.size(destination_file) == 0)) {
        loginfo("%s downloading to %s", covariate["name"], covariate["destfile"])
        download(url = as.character(covariate["url"]), destfile = destination_file, mode = "wb")
    } else {
        logdebug("%s already exists at %s", covariate["name"], covariate["destfile"])
    }
}

# Retrive EE layer from GCS
.retrieve_earth_engine_for <- function(layer_name) {
    dates <- seq(CONFIG$start_date, (CONFIG$end_date + months(1)), by = "month")
    
    for (j in 1:length(dates)) {
        year = strftime(dates[j], "%Y")
        month = strftime(dates[j], "%m")
        ee_info = generate_EE_url(year, month, layer_name)
        .download_with_logic(list(name = ee_info$destfile, destfile = ee_info$destfile, url = ee_info$url))
    }
}

# getData is used as a source - and the required attributes are chucked into the 'destfile' field
.handle_get_data <- function(covariate) {
    # TODO: Stop using covariate's destfile as hold-all location for getData specification, but for
    # now just keep using it...
    
    params = unlist(str_split((covariate["destfile"]), "-"))
    first = params[1]  # If it's alt', then there is no second
    
    # Figure out what kind of getData we need - either alt or GADM
    if (first == "alt") {
        got_data = raster::getData(first, country = CONFIG$country, mask = T, path = get_input_files_path())
    } else if (first == "GADM") {
        # If first attr is 'gadm', then second will be a level
        level = params[2]
        got_data = raster::getData(first, country = CONFIG$country, level = level, path = get_input_files_path())
    } else {
        logwarn("No idea what kind of getData you want for %s", covariate["name"])
    }
    got_data
}


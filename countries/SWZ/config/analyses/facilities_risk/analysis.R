# ANALYSIS
# Needs to output against following path, with these outputs 
# See docs.disarm.io for more (and updated) info
# 
# :country/:ref_date/analysis/:analysis_type/current-month      headline, data, summary


.write_analysis_current_month_data <- function(filepath, analysis_name, date_index, mapped_results){
    # Create filepaths for outputs (CSV and JSON needed for API)
    output_file_json = file.path(filepath, 'data.json')
    output_file_csv = file.path(filepath, 'data.csv')

    raster = mapped_results$number_of_cases_stack[[date_index]]
    population_layer = get_data_for_covariate(CONFIG$population_layer)
    population_layer[is.na(population_layer[])] <- 0 

    # Do overlay of risk with facilities
    facilities_file_path = file.path(get_analyses_files_path(), analysis_name, 'HFs.csv')
    facilities = read.csv(facilities_file_path)
    
    # Population weighted risk in 10km buffer
    facilities_points = extract(raster, facilities[,c('Longitude', 'Latitude')], buffer=1000, fun=sum, na.rm=T)
    population_points = extract(population_layer, facilities[,c('Longitude', 'Latitude')], buffer=1000, fun=sum,na.rm=T)
    facilities$risk <- facilities_points/population_points
                       
    output_facilities = facilities[c('A0005FacilityName', 'Longitude', 'Latitude', 'risk')]
    output_facilities = rename(output_facilities, title = A0005FacilityName, lat = Latitude, lng = Longitude)
    
    # Write JSON version
    sink(output_file_json)
    cat(toJSON(unname(split(output_facilities, 1:nrow(output_facilities)))))
    sink()

    # Write CSV version
    write.csv(output_facilities, output_file_csv)

    return(output_facilities)
}

.write_analysis_current_month_headline <- function(filepath, ref_date, facilities){
    output_file = file.path(filepath, 'headline.txt')
    date_string = format(ref_date, '%B %Y')

    top_facilities <- as.character(facilities[order(facilities$risk),'title'][1:3])
    top_facilities_string = paste(top_facilities, collapse = ', ')
    
    string <- 'In %s, the 3 facilities in highest risk areas are %s'
    vals   <- c(date_string, top_facilities_string)

    text = do.call(sprintf, as.list(c(string, vals)))

    writeLines(text, output_file)

    return(text)
}

.write_analysis_current_month_summary <- function(filepath, headline){
    output_file = file.path(filepath, 'summary.txt')

    sink(output_file)
    
    cat("\n\n\n# Overview\n")
    cat("===============\n")
    cat("The 'facilities risk' model agrregates risk within a 10km buffer of each given health facility. It includes a 'title', `lat`, `lng` and `risk` field. The 'title' is a name for each point. The `risk` field represents a relative level of risk.")

    cat("\n\n\n# CONFIG\n")
    cat("===============\n")
    print(CONFIG)

    cat("\n\n\n# AppMetadata\n")
    cat("===============\n")
    print(AppMetadata)

    cat("\n\n\n# git commit:\n")
    cat("===============\n")
    print(system("git rev-parse --short HEAD", intern = TRUE))


    cat("\n\n\n# Headline text:\n")
    cat("===============\n")
    cat(headline)

    sink()
}

# TODO: Remove cases_results
run_analysis <- function(analysis_name, ref_date, date_index, mapped_results, cases_results, cases){
    loginfo(paste('Running analysis in', analysis_name, 'for', ref_date))
    filepath = generate_api_filepath(ref_date, 'analysis', analysis_name, 'current-month')

    data     = .write_analysis_current_month_data(filepath, analysis_name, date_index, mapped_results)
    headline = .write_analysis_current_month_headline(filepath, ref_date, data)
    summary  = .write_analysis_current_month_summary(filepath, headline)
    return()
}


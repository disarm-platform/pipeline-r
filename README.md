# DiSARM Pipeline-r - Processing raw cases and covariates into risk maps

At the moment this uses an LGCP approach.

## Assume it's all set up, and you want to run it from command line

`Rscript app.R {COUNTRY} {END_DATE}`

... where `COUNTRY` is ISO3 code (e.g. SWZ or ZWE) for a country the pipeline is configured for, and `END_DATE` is a 'YYYY-MM-DD'-format date that specifies the _end_ of a 12 month period you're interested in. It will figure out the actual `end_date`, as a reference date which is the first day of the most recently completed month (i.e. the previous month).

TODO: [ ] If an `END_DATE` is not given, it will use the most recent completed month according to the system clock.

## Run it from console

`source('app.R')`
`CONFIG <<- list(country = "SWZ", given_end_date = "2015-12-31")`
`create_config()`
`app_recipe()` (or step through `app_recipe()` by hand)

## Prepare deployment machine
Use Ubuntu. (Debian Jessie had some weird error on the Mesh step.)

Maybe add `tmux`, and definitely `git` if you're 'git cloning'.

Get some compilers

- `sudo apt-get update`
- `sudo apt-get install build-essential libgdal-dev libproj-dev` (for rgdal)
- `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9`
- edit `/etc/apt/sources.list` to add something like `deb https://<my.favorite.cran.mirror>/bin/linux/ubuntu xenial/` eg. `deb http://ftp5.gwdg.de/pub/misc/cran/bin/linux/ubuntu xenial/`
- `sudo apt-get install r-base`

- `watch -n 5 'ps -ax -o etime,command,pid -c | grep R'`

### Dependencies

Install them with:
```R
install.packages(c('nlme', 'downloader', 'shapefiles', 'stringr', 'raster', 'deldir', 'sp', 'lubridate', 'logging', 'rgdal', 'rjson', 'dplyr', 'mgcv', 'devtools'))
```

And for ppmify...
```
devtools::install_github('goldingn/ppmify')
# or devtools::install_bitbucket('disarm-platform/ppmify')
```

## Country config and files

### Files list

A list of covariates and other files places in `countries/{ISO3}/config/files.csv`, which should have fields as follows:

- name: {string} Used as identifier, but turned into snake_case on load
- source: {string} Choice of `url` (download), `getData` (use `getData` with added attributes in the `destfile` col) or `ee` (EarthEngine)
- url: {string} Either a URL or blank
- destfile: {string} Destination file, with `url` source type gives the exact filename. For `getData` source type used store additional `getData` attributes, and destination file handled by `getData`. For `ee` source type is used to generate filename.
- type: {string} _Data_ type, choice of `csv`, `rds`, `raster` or `shapefile`
- covariate: {bool} Whether this is a covariate, and e.g. included in covariate inputs to the model.
- monthly: {bool} Whether this contains monthly values, or a single value.


#### Required files:

A country requires a minimum of cases and population layers.

### Configuration
Each country needs some custom configuration placed in `countries/{ISO3}/config/config.R`, and should look like this:

```R
list(
  target_resolution_layer = 'swaziland_population',
  population_layer        = 'swaziland_population',
  boundary_layer          = 'swaziland_boundary',
  bounding_box            = c(30.7875, -27.3099609375, 32.112890625, -25.74296875)
)
```

- target_resolution_layer: {string} Must match the snake_case version of a file in `files.csv` above
- population_layer: {string} Must match the snake_case version of a file in `files.csv` above
- boundary_layer: {string} Must match the snake_case version of a file in `files.csv` above
- bounding_box: {list} returned as (lon1, lat1, lon2, lat2) (see below)

#### Bounding box
Can grab from [http://boundingbox.klokantech.com/](http://boundingbox.klokantech.com/), using the "CSV" output - can paste straight into the config line: `c(_INSERT CSV PASTE HERE_)`


## Steps for building a Generic Model

0. Prepare inputs (also done elsewhere) `prepare_input_files.R`
    - requires list of files as '{COUNTRY}_files.csv'
    - downloads missing files
    - returns AppData and AppMetadata

1. Build a mesh 
    - requires Cases (our TBD Cases format)
    - requires Boundary (Shapefile)
    - requires time_knots (Integer)
    - and nothing else
    - returns a Mesh (temporal and spatial)
    - returns a SpatialPolygon
    - returns a AST
    
2. Extract covariate values at mesh and case locations
    some are dynamic (change per month), others are static (don't change)
    - requires a Mesh (temporal and spatial)
    - requires Covariate rasters (dynamic and static)
    - requires Cases locations
    - and nothing else
    - returns Vectors of extracted covariate values at mesh and case locations

3. Calculates population across Mesh
    - requires a Mesh (spatial and temporal)
    - requires Population
    - requires SpatialPolygon
    - and nothing else
    - returns Vector of populations in each Mesh tile

4. Setup model
    - requires a Mesh (spatial and temporal)
    - requires output Vectors from Step#2
    - requires output Vector from Step#3
    - requires time_knots
    - requires Cases
    - returns a bunch of Model inputs

5. **Run model**
    - requires Model inputs from Step#4
    - returns a Result object

6. Mapping result
    - requires a SpatialPolygon
    - requires a Result object
    - requires a Mesh (spatial and temporal)
    - requires Covariate rasters
    - requires time_knots
    - returns RasterStacks
    - returns AnnualRaster

7. Save output
    - requires a RasterStacks
    - requires AnnualRaster
    - requires Population
    - saves files
    - returns NOTHING - you are finished!


## Logging

Using `library(logging)` from [here](http://logging.r-forge.r-project.org/sample_session.php)

This creates a timestamped file in the `log` folder. If the model runs correctly, it will also produce output in the relevant `_country_/output` folder.

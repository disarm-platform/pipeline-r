# Template for DiSARM pipeline analysis
# 
# Copy this file to `countries/{COUNTRY}/config/analyses/{ANALYSIS_NAME}.R`
# 
# It needs to write at least 3 outputs: headline, data, summary
# 
# See docs.disarm.io for more (and updated) info on what the functions need to include
# to keep API users happy!
# 
# Go ahead and fill in the blanks in each of the 3 methods below...


.write_analysis_current_month_data <- function(filepath, analysis_name, date_index, mapped_results){
    # Create filepaths for outputs (CSV and JSON needed for API)
    output_file_json = file.path(filepath, 'data.json')
    output_file_csv = file.path(filepath, 'data.csv')

    #                               #
    #  INSERT CUSTOM FUNCTIONALITY  #
    #                               #

    return(facilities)
}

.write_analysis_current_month_headline <- function(filepath, ref_date, facilities){
    output_file = file.path(filepath, 'headline.txt')
    date_string = format(ref_date, '%B %Y')

    #                               #
    #  INSERT CUSTOM FUNCTIONALITY  #
    #                               #

    return(text)
}

.write_analysis_current_month_summary <- function(filepath, headline){
    output_file = file.path(filepath, 'summary.txt')

    sink(output_file)
    
    cat("Analysis summary text")
    #                               #
    #  INSERT CUSTOM FUNCTIONALITY  #
    #                               #

    sink()
}

run_analysis <- function(analysis_name, ref_date, date_index, mapped_results, cases_results, cases){
    loginfo(paste('Running analysis in', analysis_name, 'for', ref_date))
    filepath = generate_api_filepath(ref_date, 'analysis', analysis_name, 'current-month')

    data     = .write_analysis_current_month_data(filepath, analysis_name, date_index, mapped_results)
    headline = .write_analysis_current_month_headline(filepath, ref_date, data)
    summary  = .write_analysis_current_month_summary(filepath, headline)
    return()
}

